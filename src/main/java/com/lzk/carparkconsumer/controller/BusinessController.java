package com.lzk.carparkconsumer.controller;

import app.common.pojo.Business;
import app.common.utils.Dto;
import com.github.pagehelper.PageInfo;

import com.lzk.carparkconsumer.service.BusinessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("/business")
public class BusinessController {
    @Autowired
    private BusinessService businessService;
    @RequestMapping("/sB")
    Dto<PageInfo<Business>> selB(@RequestParam int page,@RequestParam int limit,@RequestParam(required = false) String account,@RequestParam(required = false) String type,@RequestParam(required = false) String t1){
        return businessService.selB(page,limit,account,type,t1);
    }
}
