package com.lzk.carparkconsumer.controller;

import app.common.pojo.Suggestion;
import app.common.utils.Dto;
import com.github.pagehelper.PageInfo;
import com.lzk.carparkconsumer.service.SuggestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/Suggestion")
public class SuggestionController {
    @Autowired
    private SuggestionService suggestionService;
    @RequestMapping("/sS")
    Dto<PageInfo<Suggestion>> selS(@RequestParam int page,@RequestParam int limit,@RequestParam(required = false) String title,@RequestParam(required = false) String account,@RequestParam(required = false) String t1){
        return suggestionService.selS(page,limit,title,account,t1);
    }
    @RequestMapping("/sB")
    Dto<Suggestion> selByid(@RequestParam int id){
        return suggestionService.selByid(id);
    }
    @RequestMapping("/uS")
    int updS(@RequestParam int id,@RequestParam String feedBack){
        return suggestionService.updS(id,feedBack);
    }
}
