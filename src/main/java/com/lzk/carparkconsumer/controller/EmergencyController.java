package com.lzk.carparkconsumer.controller;


import app.common.pojo.Emergency;
import app.common.utils.Dto;
import com.github.pagehelper.PageInfo;
import com.lzk.carparkconsumer.service.EmergencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("/emergency")
public class EmergencyController  {

    @Autowired
    private EmergencyService emergencyService;
    @RequestMapping("sB")
    public Dto<Emergency> selByid(@RequestParam int id) {
        return emergencyService.selByid(id);
    }
    @RequestMapping("sA")
    public Dto<PageInfo<Emergency>> selA(@RequestParam("page") int pageNum,@RequestParam("limit") int pageSize,@RequestParam(required = false) String title,@RequestParam(required = false) String t1,@RequestParam(required = false) String t2) {
        return emergencyService.selA(pageNum,pageSize,title,t1,t2);
    }
    @RequestMapping("iE")
    public int insE(@RequestBody Emergency emergency) {
        return emergencyService.insE(emergency);
    }
    @RequestMapping("uE")
    public int updE(@RequestBody Emergency emergency) {
        return emergencyService.updE(emergency);
    }
    @RequestMapping("dE")
    public int delE(@RequestParam int id) {
        return emergencyService.delE(id);
    }
}
