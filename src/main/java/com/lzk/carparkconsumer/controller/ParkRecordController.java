package com.lzk.carparkconsumer.controller;

import app.common.pojo.ParkRecord;
import app.common.utils.Dto;
import com.github.pagehelper.PageInfo;

import com.lzk.carparkconsumer.service.ParkRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/ParkRecord")
public class ParkRecordController{
    @Autowired
    private ParkRecordService parkRecordService;
    @RequestMapping("sp")
    public Dto<PageInfo<ParkRecord>> selP(@RequestParam("page") int page, @RequestParam("limit") int limit, @RequestParam(required = false) Integer cid, @RequestParam(required = false) String carNumber, @RequestParam(required = false) Integer type, @RequestParam(required = false) String t1, @RequestParam(required = false) String t2) {
        return parkRecordService.selP(page,limit,cid,carNumber,type,t1,t2);
    }
}
