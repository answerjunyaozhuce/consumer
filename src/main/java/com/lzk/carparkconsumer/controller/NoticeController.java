package com.lzk.carparkconsumer.controller;

import app.common.api.NoticeApi;
import app.common.pojo.Notice;
import app.common.utils.Dto;
import com.github.pagehelper.PageInfo;
import com.lzk.carparkconsumer.service.NoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/notice")
public class NoticeController implements NoticeApi {

    @Autowired
    private NoticeService noticeService;


    @RequestMapping("sB")
    public Dto<Notice> selByid(int id) {
        return noticeService.selByid(id);
    }

    @RequestMapping("slA")
    public Dto<PageInfo<Notice>> selA(@RequestParam("page") int pageNum,@RequestParam("limit") int pageSize,@RequestParam(required = false,value = "title") String title,@RequestParam(required = false,value = "t1") String t1,@RequestParam(required = false,value = "t2") String t2) {
        return noticeService.selA(pageNum,pageSize,title,t1,t2);
    }

    @RequestMapping("iN")
    public int insN(@RequestBody Notice notice) {
        return noticeService.insN(notice);
    }

    @RequestMapping("uN")
    public int updN(@RequestBody Notice notice) {
        return noticeService.upd(notice);
    }

    @RequestMapping("dN")
    public int delN(int id) {
        return noticeService.del(id);
    }
}
