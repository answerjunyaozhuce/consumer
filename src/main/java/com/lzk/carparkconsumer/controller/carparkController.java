package com.lzk.carparkconsumer.controller;


import app.common.pojo.CarPark;
import app.common.utils.Dto;
import com.github.pagehelper.PageInfo;
import com.lzk.carparkconsumer.service.carparkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class carparkController{
    @Autowired
    private carparkService carparkService;
        @RequestMapping("/carparkList")
        public Dto<List<CarPark>> carparkList() {
        return carparkService.carparkList();
    }


    @RequestMapping("/pl")
    Dto<PageInfo<CarPark>> carparkPage(@RequestParam int page,@RequestParam int limit,@RequestParam(required = false) Integer cid,@RequestParam(required = false) Integer type){
        return carparkService.carparkPage(page,limit,cid,type);
    }
}
