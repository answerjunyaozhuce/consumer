package com.lzk.carparkconsumer.controller;

import app.common.pojo.parkTotal;
import app.common.utils.Dto;
import com.lzk.carparkconsumer.service.parkTotalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/parkTotal")
public class parkTotalController {
    @Autowired
    private parkTotalService parkTotalService;
    @RequestMapping("/sT")
    Dto<List<parkTotal>> selpT(@RequestParam String time){
        return parkTotalService.selpT(time);
    }
}
