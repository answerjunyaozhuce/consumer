package com.lzk.carparkconsumer.client;

import app.common.api.CarparkApi;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "carparkproduct")
public interface carparkClient extends CarparkApi {
}
