package com.lzk.carparkconsumer.client;

import app.common.api.SuggestionApi;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "carparkproduct")
public interface SuggestionClient extends SuggestionApi {
}
