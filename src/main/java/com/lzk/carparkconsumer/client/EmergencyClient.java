package com.lzk.carparkconsumer.client;

import app.common.api.EmergencyApi;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "carparkproduct")
public interface EmergencyClient extends EmergencyApi {
}
