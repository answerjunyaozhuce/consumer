package com.lzk.carparkconsumer.client;

import app.common.api.NoticeApi;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "carparkproduct")
public interface NoticeClient extends NoticeApi {
}
