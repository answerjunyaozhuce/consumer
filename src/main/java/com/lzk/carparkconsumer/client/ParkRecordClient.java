package com.lzk.carparkconsumer.client;

import app.common.api.ParkRecordApi;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "carparkproduct")
public interface ParkRecordClient extends ParkRecordApi {
}
