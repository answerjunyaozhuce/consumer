package com.lzk.carparkconsumer.client;

import app.common.api.BusinessApi;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "carparkproduct")
public interface BusinessClient extends BusinessApi {
}
