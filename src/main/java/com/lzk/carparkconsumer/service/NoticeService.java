package com.lzk.carparkconsumer.service;

import app.common.pojo.Notice;
import app.common.utils.Dto;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface NoticeService {

    Dto<Notice> selByid(int id);

    Dto<PageInfo<Notice>> selA(int pageNum,int pageSize,String title, String t1, String t2);

    int insN(Notice notice);

    int upd(Notice notice);

    int del(int id);

}
