package com.lzk.carparkconsumer.service;

import app.common.pojo.ParkRecord;
import app.common.utils.Dto;
import com.github.pagehelper.PageInfo;

public interface ParkRecordService {
    public Dto<PageInfo<ParkRecord>> selP(int page, int limit, Integer cid, String carNumber, Integer type, String t1, String t2);
}
