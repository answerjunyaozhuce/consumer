package com.lzk.carparkconsumer.service;

import app.common.pojo.Emergency;
import app.common.utils.Dto;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


public interface EmergencyService {

    Dto<Emergency> selByid(int id);


    Dto<PageInfo<Emergency>> selA( int pageNum, int pageSize, String title, String t1, String t2);


    int insE(Emergency emergency);


    int updE(Emergency emergency);


    int delE(int id);
}
