package com.lzk.carparkconsumer.service;

import app.common.pojo.CarPark;
import app.common.utils.Dto;
import com.github.pagehelper.PageInfo;


import java.util.List;

public interface carparkService {
    public Dto<List<CarPark>> carparkList();

    public Dto<PageInfo<CarPark>> carparkPage(int page,int limit,Integer cid,Integer type);

}
