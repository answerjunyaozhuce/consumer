package com.lzk.carparkconsumer.service;

import app.common.pojo.Business;
import app.common.utils.Dto;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.RequestParam;

public interface BusinessService {
    Dto<PageInfo<Business>> selB(@RequestParam int page, @RequestParam int limit, @RequestParam(required = false) String account, @RequestParam(required = false) String type, @RequestParam(required = false) String t1);

}
