package com.lzk.carparkconsumer.service.Impl;

import app.common.pojo.Notice;

import app.common.utils.Dto;
import com.github.pagehelper.PageInfo;
import com.lzk.carparkconsumer.client.NoticeClient;
import com.lzk.carparkconsumer.service.NoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class NoticeServiceImpl implements NoticeService {
    @Autowired
    private NoticeClient noticeClient;



    @Override
    public Dto<Notice> selByid(int id) {
        return noticeClient.selByid(id);
    }

    @Override
    public Dto<PageInfo<Notice>> selA(int pageNum, int pageSize, String title, String t1, String t2) {
        return noticeClient.selA(pageNum,pageSize,title,t1,t2);
    }

    @Override
    public int insN(Notice notice) {
        return noticeClient.insN(notice);
    }

    @Override
    public int upd(Notice notice) {
        return noticeClient.updN(notice);
    }

    @Override
    public int del(int id) {
        return noticeClient.delN(id);
    }
}
