package com.lzk.carparkconsumer.service.Impl;

import app.common.pojo.CarPark;
import app.common.utils.Dto;
import com.github.pagehelper.PageInfo;
import com.lzk.carparkconsumer.client.carparkClient;
import com.lzk.carparkconsumer.service.carparkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class carparkServiceImpl implements carparkService {
    @Autowired
    private carparkClient carparkClient;
    @Override
    public Dto<List<CarPark>> carparkList() {
        return carparkClient.carparkList();
    }

    @Override
    public Dto<PageInfo<CarPark>> carparkPage(int page, int limit, Integer cid, Integer type) {
        Map<String,Object> map=new HashMap<>();
        map.put("page",page);
        map.put("limit",limit);
        map.put("cid",cid);
        map.put("type",type);

        return carparkClient.carparkPage(map);
    }
}
