package com.lzk.carparkconsumer.service.Impl;

import app.common.pojo.Business;
import app.common.utils.Dto;
import com.github.pagehelper.PageInfo;
import com.lzk.carparkconsumer.client.BusinessClient;
import com.lzk.carparkconsumer.service.BusinessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class BusinessServiceImpl implements BusinessService {
    @Autowired
    private BusinessClient businessClient;
    @Override
    public Dto<PageInfo<Business>> selB(int page, int limit, String account, String type, String t1) {
        Map<String,Object> map=new HashMap<>();
        map.put("page",page);
        map.put("limit",limit);
        map.put("account",account);
        map.put("type",type);
        map.put("t1",t1);

        return businessClient.selB(map);
    }
}
