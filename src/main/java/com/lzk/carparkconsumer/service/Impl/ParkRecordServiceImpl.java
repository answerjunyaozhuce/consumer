package com.lzk.carparkconsumer.service.Impl;

import app.common.pojo.ParkRecord;
import app.common.utils.Dto;
import com.github.pagehelper.PageInfo;
import com.lzk.carparkconsumer.client.ParkRecordClient;
import com.lzk.carparkconsumer.service.ParkRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ParkRecordServiceImpl implements ParkRecordService {
    @Autowired
    private ParkRecordClient parkRecordClient;
    @Override
    public Dto<PageInfo<ParkRecord>> selP(int page, int limit, Integer cid, String carNumber, Integer type, String t1, String t2) {
        return parkRecordClient.selP(page,limit,cid,carNumber,type,t1,t2);
    }
}
