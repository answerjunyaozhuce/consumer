package com.lzk.carparkconsumer.service.Impl;

import app.common.pojo.Suggestion;
import app.common.utils.Dto;
import com.github.pagehelper.PageInfo;
import com.lzk.carparkconsumer.client.SuggestionClient;
import com.lzk.carparkconsumer.service.SuggestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class SuggestionServiceImpl implements SuggestionService {
    @Autowired
    private SuggestionClient suggestionClient;
    @Override
    public Dto<PageInfo<Suggestion>> selS(int page, int limit, String title, String account, String t1) {
        Map<String,Object> map=new HashMap<>();
        map.put("page",page);
        map.put("limit",limit);
        map.put("account",account);
        map.put("title",title);
        map.put("t1",t1);

        return suggestionClient.selS(map);
    }

    @Override
    public Dto<Suggestion> selByid(int id) {
        return suggestionClient.selByid(id);
    }

    @Override
    public int updS(int id, String feedBack) {
        return suggestionClient.updS(id,feedBack);
    }
}
