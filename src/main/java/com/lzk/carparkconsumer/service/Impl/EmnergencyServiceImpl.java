package com.lzk.carparkconsumer.service.Impl;

import app.common.pojo.Emergency;
import app.common.utils.Dto;
import com.github.pagehelper.PageInfo;
import com.lzk.carparkconsumer.client.EmergencyClient;
import com.lzk.carparkconsumer.service.EmergencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmnergencyServiceImpl implements EmergencyService {
    @Autowired
    private EmergencyClient emergencyClient;
    @Override
    public Dto<Emergency> selByid(int id) {
        return emergencyClient.selByid(id);
    }

    @Override
    public Dto<PageInfo<Emergency>> selA(int pageNum, int pageSize, String title, String t1, String t2) {
        return emergencyClient.selA(pageNum,pageSize,title,t1,t2);
    }

    @Override
    public int insE(Emergency emergency) {
        return emergencyClient.insE(emergency);
    }

    @Override
    public int updE(Emergency emergency) {
        return emergencyClient.updE(emergency);
    }

    @Override
    public int delE(int id) {
        return emergencyClient.delE(id);
    }
}
