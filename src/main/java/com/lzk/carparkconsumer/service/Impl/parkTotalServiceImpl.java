package com.lzk.carparkconsumer.service.Impl;

import app.common.pojo.parkTotal;
import app.common.utils.Dto;
import com.lzk.carparkconsumer.client.parkTotalClient;
import com.lzk.carparkconsumer.service.parkTotalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class parkTotalServiceImpl implements parkTotalService {
    @Autowired
    private parkTotalClient parkTotalClient;
    @Override
    public Dto<List<parkTotal>> selpT(String time) {
        return parkTotalClient.selpT(time);
    }
}
