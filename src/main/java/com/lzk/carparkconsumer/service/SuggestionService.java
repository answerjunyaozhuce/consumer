package com.lzk.carparkconsumer.service;

import app.common.pojo.Suggestion;
import app.common.utils.Dto;
import com.github.pagehelper.PageInfo;


public interface SuggestionService {
    Dto<PageInfo<Suggestion>> selS(int page,int limit,String title,String account,String t1);

    Dto<Suggestion> selByid(int id);

    int updS(int id,String feedBack);
}
